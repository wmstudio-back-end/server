'use strict';
var MongoClient = require('mongodb').MongoClient
exports.Connect = function(callback){
        // let url = 'taxi:taxi@localhost:27017/taxi'
    let url = 'taxi:taxi@193.189.89.145:27017/taxi'
        MongoClient.connect('mongodb://'+url,{ useNewUrlParser: true }, function(err, db) {
            if (err){
                console.log(err)
                callback('Module Database: [ERROR]', null);
                return;
            }
            db = db.db('taxi')
            global.db.drivers = db.collection('drivers');
            global.db.admins = db.collection('admins');
            global.db.orders = db.collection('orders');
            global.db.gpslog = db.collection('gpslog');
            callback(null, 'Module Database: [OK]');

        })
}


