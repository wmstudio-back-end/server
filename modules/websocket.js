var WebSocketServer = require('ws').Server
    , http = require('http')
    , express = require('express')
    , app = express()
    , async       = require('async')
    , port = process.env.PORT || 7001;
exports.Init = function(callback){
    app.use(express.static(__dirname + '/'));
    var server = http.createServer(app);
    server.listen(port);
    var wss = new WebSocketServer({server: server});
    wss.on('connection', function connection(ws) {
        ws.send(JSON.stringify({method:'EVENT/options',data:{typeAvto:typeAvto},error:null,requestId:-1}));
        ws.on('message',  onMessage);
        ws.on('close',() => {
            if (ws._id&&USERS[ws._id]){
                delete USERS[ws._id]
            }
        });
        ws.on('finish',() => {
            if (ws._id&&USERS[ws._id]){
                delete USERS[ws._id]
            }
            ws.destroy();
        });
        ws.on('error', (error) => {
            if (ws._id&&USERS[ws._id]){
                delete USERS[ws._id]
            }
        });
    });
    callback(null, 'Module Websocket: [OK]');
}

onMessage = function(res){
    var $this = this
    var r = false
    var msg = JSON.parse(res)
    if (msg.hasOwnProperty('method')&&controllers.hasOwnProperty(msg.method)){
        if (!this._id){
            if (msg.method=='authDriver'||msg.method=='authAdmin'){
                r = true
                console.log("MESSAGE",msg.method)
            }
        }else{r = true}
        if (r){
            controllers[msg.method](msg.data,function(err,data){
                msg.data = data
                msg.error = err
                $this.send(JSON.stringify(msg));
            },$this);
        }

    }else{
        console.log('NO METHOD');
    }

}
