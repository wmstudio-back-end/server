const { Order } = require('./server_order')
const { Driver } = require('./server_driver')
require('paint-console')
require('async').series([
      require('./modules/database').Connect,
      require('./modules/cache').Init,
      require('./modules/websocket').Init,



],(err, results)=>err?console.error(err):console.info(results.join('\n'))
)


console.info('console.info();');
console.warn('console.warn();');
console.error('console.error();');
console.log('console.log();');
ObjectID = require('mongodb').ObjectID
ObjectId = require('mongodb').ObjectID
global.db = {};
USERS = {}
ORDERS = []



controllers = {
  getHistoryDriver:function(msg,callback,ws){
    if (USERS[ws._id]) {
      global.db.orders.find({driver:ws._id}).toArray(function(err,data){
        callback(err,{orders:data})
      })
    }
  },
  getDrivers:function(msg,callback,ws){

      global.db.drivers.find({}).toArray(function(err,data){
        callback(err,{drivers:data})
      })


  },
  sendStatusOrder:function(msg,callback,ws){
    if (USERS[ws._id]) {
      if (USERS[ws._id].order){
        USERS[ws._id].order.sendStatusOrder(msg)
      }

    }
    callback(null,{data:true})
  },
  getStatuses:function(ХЪ,callback,ws){
    let drivers = []
    for (var key in USERS) {
      let dr = {
        profile:USERS[key].profile,
        order:USERS[key].order?USERS[key].order.order:null
      }
      drivers.push(dr)
    }
    let orders = []
    for (var key in ORDERS) {

      orders.push(ORDERS[key].order)
    }
    callback(null,{drivers:drivers,orders:orders})
  },
  finishOrderDrive:function(msg,callback,ws){
    if (USERS[ws._id]) {
      USERS[ws._id].order.finishOrderDrive(msg,callback)
    }
  },
  readyTostartDrive:function(msg,callback,ws){
  if (USERS[ws._id]) {
    USERS[ws._id].order.readyTostartDrive(callback)
  }
  },
  applyOrder:function(msg,callback,ws){
  if (USERS[ws._id]){
    USERS[ws._id].applyOrder(callback)
  }
},
  cancelOrder:function(msg,callback,ws){
  if (USERS[ws._id]){
    USERS[ws._id].cancelOrder(callback)
  }
  },
  setStatusOnline:function(msg,callback,ws){
    if (USERS[ws._id]){
      USERS[ws._id].profile.online = msg.online
      callback(null,{result:true})
    }
  },
  getHistory: function(msg,callback,ws){
    global.db.orders.find({}).toArray(function(err,data){
      callback(err,data)
    })

  },
  setGPSDriver : function(msg,callback,ws){
    if (USERS[ws._id]){
      USERS[ws._id].profile.gps = msg.gps
      // console.log('UPDATE GPS',ws._id)
      global.db.gpslog.save({uid:ws._id,gps:msg.gps},function(err,data){
        // console.log(err,data)
      })
      callback(null,{result:true})
    }else{
      console.log('NO USER setGPSDriver',ws._id)
    }

  },
  authAdmin : function(msg,callback,ws){
    if (msg.hasOwnProperty('code')){
      global.db.admins.findOne({code:msg.code},function(err,data){
        console.log(data)
        if (data){
          ws._id = data._id
          callback(err,data)
        }else{

          callback({err:'notfound'},null)
        }


      })
    }
    if (msg.hasOwnProperty('token')){
      global.db.admins.findOne({_id:ObjectId(msg.token)},function(err,data){
        if (data){
          ws._id = data._id
        }

        callback(err,data)

      })
    }
  },
  authDriver : function(msg,callback, ws){

    if (msg.hasOwnProperty('numberDriver')){
      console.log('SSSSSSSSSSSSSS')
      global.db.drivers.findOne({numberDriver:msg.numberDriver},function(err,data){
        if (data){

          ws._id = data._id
          new Driver(data,ws,callback)

        }else{
          if (msg.name&&msg.typeAvto&&msg.numberAvto&&msg.markAvto&&msg.numberDriver&&msg.phone){


          var newUser = {
            name:msg.name,
            type:[parseInt(msg.typeAvto)],
            number:msg.numberAvto,
            avto:msg.markAvto,
            numberDriver:msg.numberDriver,
            phone:msg.phone,
            online : false
          }
          global.db.drivers.insertOne(newUser,function(err,data,is){
            ws._id = data['ops'][0]._id
            new Driver(data['ops'][0],ws,callback)


          })
          }else {

            console.log('NOT DATA',msg)
          }
        }
      })
    }
    if (msg.hasOwnProperty('token')){
      console.log(msg.token)
      global.db.drivers.findOne({_id:ObjectId(msg.token)},function(err,data){
        if (data){
          ws._id = data._id
          new Driver(data,ws,callback)

        }else{
          console.log('NOTOKEN')
          callback(err,null)
        }
      })
    }
  },
  calculateOrder:function(msg,callback=function(){}){
    var price = 0


    switch (msg.tariff) {
      case 1:

        if (msg.time<3600){msg.time = 0;}
        else{
          msg.time -=3600
        }
        console.log('меньше часа',msg.time)
        var o = (msg.time/3600)
        console.log('msg.time-3600/3600',o)
        price = typeAvto[msg.type].firstHour
        console.log('price',price)
        price += o*typeAvto[msg.type].nextHour
        break
      case 2:
        price = typeAvto[msg.type].supply
        price += (msg.length/1000)*typeAvto[msg.type].kilometer
        break;
      case 3:
        price = typeAvto[msg.type].supply
        price += (msg.length/1000)*typeAvto[msg.type].kilometerback
        break;
    }
    callback(null,{price:price})
    return price
  },
  createInitOrder:function(msg,callback,ws){

    var price = controllers.calculateOrder((Object.assign({},msg)))
    // 0 - заявка создана
    // 1 - заявка отправлена водителю
    // 2 - заявка на исполнении водителем
    // 3 - заявка завершена

    msg.status = 0
    msg.priceStart = price
    msg.created_at = new Date()
    msg.admin_id = ObjectId(ws._id)
    msg.path[0].coordinates[0] = parseFloat(msg.path[0].coordinates[0].toFixed(10))
    msg.path[0].coordinates[1] = parseFloat(msg.path[0].coordinates[1].toFixed(10))

    msg.path[1].coordinates[0] = parseFloat(msg.path[1].coordinates[0].toFixed(10))
    msg.path[1].coordinates[1] = parseFloat(msg.path[1].coordinates[1].toFixed(10))


    ORDERS.push(new Order(msg))
    callback(null,{response:msg})



  },
  restartServer:function(msg,callback,ws){
    process.abort()

  },
  createInitOrder2:function(msg,callback,ws){

    var t = {

      "length" : 186799.57,
      "time" : 10042.04,
      "tariff" : msg.tariff,
      "type" : USERS[ws._id].profile.type[0],
      "path" : [
        {
          "address" : "Россия, Удмуртская Республика, Ижевск, улица Василия Чугуевского, 9",
          "coordinates" : [53.20677233660137,56.832112590395056]
        },
        {
          "address" : "Россия, Кировская область, Кильмезский район",
          "coordinates" : [
            51.0313992816742,
            56.9678571771953
          ]
        }
      ],
      "status" : 0,
      "priceStart" : 6924.78452,
      "created_at" : new Date(),
      "driver": ws._id
    }



    ORDERS.push(new Order(t))

    callback(null,t)

  }

}

findNearUser = function(lat,lon){
  console.log("findNearUser")
  var min = {
    uid:null,
    dist:999999999999
  }
  for (var key in USERS) {
    console.log(USERS[key].profile)
    if (USERS[key].profile&&USERS[key].profile.gps){
      USERS[key].profile.gps.lat
      USERS[key].profile.gps.lon
      var mx=Math.abs(USERS[key].profile.gps.lat-lat);
      var my=Math.abs(USERS[key].profile.gps.lon-lon);
      var dist=Math.sqrt(Math.pow(mx,2)+Math.pow(my,2));
      if (min.dist>dist){
        min.uid = key
        min.dist = dist
      }
      if (min.uid){
        return min.uid

        // console.log('SEND!!!!!!!!!!!!!!!!!!!!!!',USERS[min.uid].ws)
      }

      console.log("DIST!!!!",dist)
    }

  }
  return null
  console.log('RES', min)

}
typeAvto = {
  "1":{
    name:"каблук",
    maxWeight:600,
    maxLength:200,
    firstHour:500,
    nextHour:400,
    kilometer:20,
    kilometerback:20,
    supply:200

  },
  "2":{
    name:"Газель 3м",
    maxWeight:1500,
    maxLength:3000,
    firstHour:700,
    nextHour:600,
    kilometer:36,
    kilometerback:18,
    supply:200
  },
  "3":{
    name:"Газель 4м",
    maxWeight:1500,
    maxLength:4000,
    firstHour:800,
    nextHour:600,
    kilometer:36,
    kilometerback:18,
    supply:200
  },
  "4":{
    name:"Газель 6м",
    maxWeight:1500,
    maxLength:6000,
    firstHour:900,
    nextHour:600,
    kilometer:36,
    kilometerback:18,
    supply:200
  },
  "5":{
    name:"Газ 5 тонн",
    maxWeight:3000,
    maxLength:5000,
    firstHour:1100,
    nextHour:800,
    kilometer:40,
    kilometerback:20,
    supply:200
  }
}
