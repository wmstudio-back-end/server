'use strict';

class Order  {
  constructor(data) {
    this.order = {}
    this.TIME_UPDATE_FIND = 3000
    this.TIME_TO_FIND_DRIVER = 10000
    this.timerFind = null
    this.timerToApplyOrder = null

    if (data.hasOwnProperty('_id')){
      this.order = data
      if (this.order.status==0){//инициализация поиска машины
        this.findDriverForType()
      }
      if (this.order.status==1){//инициализация таймера, когда пользователь примет или откажется от заказа
        this.timerToApplyOrder = setTimeout(this.restartDriver.bind(this),this.TIME_UPDATE_FIND)
      }

    }else{
      this.saveOrder(data)
    }


  }

}

Order.prototype.sendStatusOrder = function(msg,callback){
  this.order.taxometr = msg['taxometr']
  global.db.orders.updateOne({_id:ObjectId(this.order._id)},{$set:{taxometr:this.order.taxometr}})
}
Order.prototype.startWorkOrder = function(){
  console.log('водитель начал выполнять заказ')
  let t = new Date()
  this.order.status = 2
  this.order.startWork = t
  global.db.orders.updateOne({_id:ObjectId(this.order._id)},{$set:{startWork:t,status:2}})
  clearTimeout(this.timerToApplyOrder)
}
Order.prototype.saveOrder = function(msg){
  console.log('SAVEEEEEEEEEEEEEEEE',msg)
  msg.taxometr = {
    way : 0,
    timeStay : 0,
    price : 0,
    time : 0
  }
  global.db.orders.insertOne(msg,function(err,data){
    if (data&&data.hasOwnProperty('ops')&&data['ops'][0]){
      this.order = data['ops'][0]
      this.findDriverForType()
    }


  }.bind(this))
}
Order.prototype.restartDriver = function(){
  console.log('отмена заказа для водителя')
  if (USERS[this.order.driver]){
    USERS[this.order.driver].cancelOrder(function(){    })
  }else{
    setTimeout(this.restartOrder.bind(this),5000)
  }

  // global.db.orders.updateOne({_id:ObjectId(this.order._id)},{$set:{driver:null,status:0}})
  // this.findDriverForType();
}
Order.prototype.restartOrder = function(){
  this.order.driver = null
  global.db.orders.updateOne({_id:ObjectId(this.order._id)},{$set:{driver:null,status:0}})
  this.findDriverForType();
}


Order.prototype.findDriverForType = function(){
  console.log('ПОИСК: '+this.order.path[0].address,this.order.type)
  var isDriver = []

// if (!this.order.driver){
  for (var key in USERS) {

    if (
      USERS[key].profile&&
      USERS[key].profile.gps&&
      USERS[key].profile.type.indexOf(this.order.type)>-1&&
      USERS[key].profile.online&&
      USERS[key].order==null
    ){
      isDriver.push({_id:key,dist:getDistanceFromLatLonInKm(this.order.path[0].coordinates[1],this.order.path[0].coordinates[0],USERS[key].profile.gps.lat,USERS[key].profile.gps.lon)})
    }
  }
  isDriver.sort((prev, next) => {
    return prev.dist - next.dist
  });
// }else{
//   isDriver.push({_id:this.order.driver,dist:0})
// }


  if (isDriver.length>0){
    if (this.timerFind){
      console.log("нашлись подходящие машины, выключаем таймер")
      clearTimeout(this.timerFind)
    }
    console.log('нашли ближайшего исполнителя, отправляем ему заявку')
    try {
      this.order.status = 1
      this.order.driver = ObjectId(isDriver[0]._id)
      USERS[isDriver[0]._id].setOrder(this)//.ws.send(JSON.stringify({method:'createInitOrder',data:{order:this.order},error:null,requestId:1}))

      global.db.orders.updateOne({_id:ObjectId(this.order._id)},{$set:{driver:ObjectId(isDriver[0]._id),status:1}})
      this.timerToApplyOrder = setTimeout(this.restartDriver.bind(this),this.TIME_TO_FIND_DRIVER)


    }catch (e) {
      console.log("не удалось отправить заявку водителю, начинаем новый поиск")
      this.setTimerFind()
    }


  }else{

    this.setTimerFind()
  }

}
Order.prototype.setTimerFind = function(){
  this.timerFind = setTimeout(this.findDriverForType.bind(this),this.TIME_UPDATE_FIND)
}

Order.prototype.readyTostartDrive = function(callback){
  console.log('водитель подъехал на место и ждет клиента')
  let t = new Date()
  this.order.status = 3
  this.order.startWorkOrder = t
  global.db.orders.updateOne({_id:ObjectId(this.order._id)},{$set:{startWorkOrder:t,status:3}})
  callback(null,this.order)
}


Order.prototype.finishOrderDrive = function(data,callback){
  console.log('водитель закончил выполнение заказа')
  let t = new Date()
  this.order.taxometr = data.taxometr
  this.order.status = 4
  this.order.endWorkOrder = t
  global.db.orders.updateOne({_id:ObjectId(this.order._id)},{$set:{endWorkOrder:t,status:4}})
  USERS[this.order.driver].finishOrder(this)
  for (var i = 0; i < ORDERS.length; i++) {
    if (this.order._id==ORDERS[i].order._id){
      ORDERS.splice(i,1)
      break;
    }
  }
  console.log(ORDERS)
  console.log(USERS[this.order.driver].order)
  callback(null,this.order)
}

module.exports =  {Order}


function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
  let a =
        Math.sin((lat2-lat1)* (Math.PI/180)/2) * Math.sin((lat2-lat1)* (Math.PI/180)/2) +
        Math.cos((lat1)* (Math.PI/180)) * Math.cos((lat2)* (Math.PI/180)) *
        Math.sin((lon2-lon1)* (Math.PI/180)/2) * Math.sin((lon2-lon1)* (Math.PI/180)/2)
  return 6371 * 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

}

